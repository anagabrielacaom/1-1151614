/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import Dao.AmigoJpaController;
import Dao.Conexion;
import Dao.HorarioJpaController;
import Dto.Amigo;
import Dto.Horario;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Principal {
    Conexion con = Conexion.getConexion();

    public Amigo ingresarAmigo(Integer cod){
        try {       
            Amigo nuevo=new Amigo();
            nuevo.setCodigo(cod);
            AmigoJpaController amigoDAO=new AmigoJpaController(con.getBd());
            amigoDAO.create(nuevo);
            return nuevo;
        } catch (Exception ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }

    public boolean ingresarHorario(int jornada,int dia,Amigo nuevo){   
        try {
            Horario nuevoH=new Horario();
            nuevoH.setCodigo(nuevo);
            nuevoH.setDia(dia);
            nuevoH.setJornada(jornada);
            HorarioJpaController horarioDAO=new HorarioJpaController(con.getBd());
            horarioDAO.create(nuevoH);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }  
    
    public String Horario(int dia, int jornada){
        HorarioJpaController horarioDAO=new HorarioJpaController(con.getBd());
        String s="";
        for (int i = 0; i < horarioDAO.findHorarioEntities().size(); i++) {
            Horario h=horarioDAO.findHorarioEntities().get(i);
            if(h.getDia()==dia&&h.getJornada()==jornada){
                s+=h.getCodigo();
            }
        }
        return s;      
    }
   public static void main(String[]args){
    Principal p=new Principal();
       System.out.println(p.Horario(1, 1));
   }
    
}
