    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import Dto.Amigo;
import Principal.Principal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ACER
 */
public class Registro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       try (PrintWriter out = response.getWriter()) {
           
            Integer codigo = Integer.parseInt(request.getParameter("codigo"));
            
            Amigo amigo = (Amigo) request.getSession().getAttribute("amigo");
            Principal p=new Principal();
            
            ArrayList<String> jornada = new ArrayList<String>();
           for (int i = 1; i <10; i++) {
               String val = request.getParameter(i+"");
               if(val!=null){     
               jornada.add(val);     
               }
           }      

           if (jornada.size() == 3) {
               Amigo a= p.ingresarAmigo(codigo);
            for (int i=0;i<jornada.size();i++) {
                String[] dato = jornada.get(i).split("-");
                p.ingresarHorario(Integer.parseInt(dato[1]), Integer.parseInt(dato[0]),a);
            }
            request.getSession().setAttribute("principal", p);
            request.getRequestDispatcher("./Rf1/Jsp/registroexitoso.jsp").forward(request, response);
        } else {
               request.getSession().setAttribute("principal", p);
               request.getRequestDispatcher("./Error/Error.jsp").forward(request, response);
           }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
